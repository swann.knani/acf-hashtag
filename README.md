# ACF Hashtag Field

**Advanced Custom Fields: Hashtag Field**

Contributors: Swann KNANI
Tags: Hashtag, Instagram, Twitter
Requires at least: 5.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

_Description_

Add hashtag field type to ACF

_Compatibility_

This ACF field type is compatible with:

- ACF 5

_Changelog_

`1.0.0`

- Initial Release.
