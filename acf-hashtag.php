<?php
/**
 * Plugin Name: Advanced Custom Fields: Hashtag
 * Description: Advanced Custom Fields: Hashtag
 * Version: 1.0.0
 * Author: Swann KNANI
 * Author URI: swann.knani@gmail.com
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 */

if ( ! defined( 'ABSPATH' ) ) {

	exit;
};


if ( ! class_exists( 'ubisoft_acf_plugin_hashtag' ) ) :

	if ( ! ( defined( 'TWITTER_OAUTH_ACCESS_TOKEN' ) && defined( 'TWITTER_OAUTH_ACCESS_TOKEN_SECRET' ) && defined( 'TWITTER_CONSUMER_KEY' ) && defined( 'TWITTER_CONSUMER_SECRET' ) ) ) {

		add_action( 'admin_notices', function() {
			echo '<div class="error"><p>One of the following variable is not defined: TWITTER_OAUTH_ACCESS_TOKEN - TWITTER_OAUTH_ACCESS_TOKEN_SECRET - TWITTER_CONSUMER_KEY - TWITTER_CONSUMER_SECRET</p></div>';
		});

		return;

	}

	class ubisoft_acf_plugin_hashtag {

		private $settings;

		/**
		 * __construct
		 *
		 *  This function will setup the class functionality
		 *
		 *  @type function
		 *  @date 17/02/2016
		 *  @since 1.0.0
		 *
		 *  @paramn void
		 *  @return void
		 */
		public function __construct() {

			$this->settings = array(
				'version'          => '1.0.0',
				'url'              => plugin_dir_url( __FILE__ ),
				'path'             => plugin_dir_path( __FILE__ ),
				'twitter_url'      => 'https://api.twitter.com/1.1/statuses/show.json',
				'twitter_settings' => array(
					'oauth_access_token'        => TWITTER_OAUTH_ACCESS_TOKEN,
					'oauth_access_token_secret' => TWITTER_OAUTH_ACCESS_TOKEN_SECRET,
					'consumer_key'              => TWITTER_CONSUMER_KEY,
					'consumer_secret'           => TWITTER_CONSUMER_SECRET,
				),
			);

			add_action( 'acf/include_field_types', array( $this, 'include_field' ) );
			add_action( 'acf/register_fields', array( $this, 'include_field' ) );
		}

		/**
		 *  Include_field
		 *
		 *  This function will include the field type class
		 *
		 *  @type function
		 *  @date 17/02/2016
		 *  @since 1.0.0
		 *
		 *  @param $version $version (int) major ACF version. Defaults to 4.
		 *  @return void
		 */
		public function include_field( $version = 5 ) {

			load_plugin_textdomain( 'ubisoft', false, plugin_basename( dirname( __FILE__ ) ) . '/lang' );

			include_once 'fields/class-ubisoft-acf-field-hashtag-v' . $version . '.php';
		}

	}

	new ubisoft_acf_plugin_hashtag();


endif;

?>
