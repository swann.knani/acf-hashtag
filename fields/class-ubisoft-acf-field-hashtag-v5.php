<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
};

if ( ! class_exists( 'TwitterAPIExchange' ) ) {

	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>You must activate TwitterAPIExchange class as she is not defined.</p></div>';
	});

	return;

}

if ( ! class_exists( 'ubisoft_acf_field_hashtag' ) ) :

	class ubisoft_acf_field_hashtag extends acf_field {

		/**
		 *  __construct
		 *
		 *  This function will setup the field type data
		 *
		 *  @type function
		 *  @date 5/03/2014
		 *  @since 5.0.0
		 *
		 *  @param Array $settings n/a.
		 *  @return n/a
		 */
		public function __construct( $settings ) {

			/**
			 * Name Single word, no spaces. Underscores allowed.
			 */
			$this->name = 'hashtag';

			/*
			*  label (string) Multiple words, can include spaces, visible when selecting a field type
			*/

			$this->label = __( 'Hashtag', 'ubisoft' );

			/**
			* Category (string) basic | content | choice | relational | jquery | layout | CUSTOM GROUP NAME.
			*/
			$this->category = 'content';

			/*
			*  defaults (array) Array of default settings which are merged into the field object. These are used later in settings
			*/
			$this->defaults = array();

			/*
			*  l10n (array) Array of strings that are used in JavaScript. This allows JS strings to be translated in PHP and loaded via:
			*  var message = acf._e('hashtag', 'error');
			*/
			$this->l10n = array();

			/*
			*  settings (array) Store plugin settings (url, path, version) as a reference for later use with assets
			*/
			$this->settings = $settings;

			$this->twitter_client = new TwitterAPIExchange( $this->settings['twitter_settings'] );

			// do not delete!
			parent::__construct();

		}

		/**
		 * Render_field()
		 *
		 * Create the HTML interface for your field
		 *
		 * @param array $field the $field being edited.
		 * @return void
		 */
		public function render_field( $field ) {

			// dump( $field );

			$value = json_decode( $field['value'], true );

			$href = $value['href'];

			/*
			*  Create a simple text input using the 'font_size' setting.
			*/

			?>
			<input type="text" name="<?php echo esc_attr( $field['name'] ); ?>" value="<?php echo esc_attr( $href ); ?>" />
			<?php
		}

		/**
		 * Validate_value()
		 *
		 * This filter is used to perform validation on the value prior to saving.
		 * All values are validated regardless of the field's required setting. This allows you to validate and return
		 * messages to the user if the value is not correct
		 *
		 * @type filter
		 * @date 11/02/2014
		 * @since 5.0.0
		 *
		 * @param boolean $valid validation status based on the value and the field's required setting.
		 * @param mixed   $value the $_POST value.
		 * @param array   $field the field array holding all the field options.
		 * @param string  $input the corresponding input name for $_POST value.
		 * @return $valid
		 */
		public function validate_value( $valid, $value, $field, $input ) {

			if ( ! ( is_instagram_url( $value ) || is_twitter_url( $value ) ) ) {

				$valid = __( 'You must provide a valid Instagram or Twitter post URL.', 'ubisoft' );
				return $valid;
			}

			if ( is_instagram_url( $value ) ) {

				$post_id = extract_instagram_post_id( $value );
				$url     = generate_instagram_url_from_post_id( $post_id );
			}

			if ( is_twitter_url( $value ) ) {

				$url = $value;
			}

			$request     = wp_safe_remote_head( $url );
			$status_code = wp_remote_retrieve_response_code( $request );

			if ( is_wp_error( $request ) || ( 200 <= $status_code && 300 <= $status_code ) ) {

				$valid = __( 'Cannot retrieve the post type. Try again.', 'ubisoft' );
			}

			return $valid;

		}

		/**
		 * Update_value()
		 *
		 * This filter is applied to the $value before it is saved in the db
		 *
		 * @type filter
		 * @since 3.6
		 * @date 23/01/13
		 *
		 * @param mixed $value the value found in the database.
		 * @param mixed $post_id the $post_id from which the value was loaded.
		 * @param array $field the field array holding all the field options.
		 * @return $value.
		 */
		public function update_value( $value, $post_id, $field ) {

			if ( is_instagram_url( $value ) ) {

				$post_id = extract_instagram_post_id( $value );
				$url     = generate_instagram_url_from_post_id( $post_id );

				$request = wp_safe_remote_get( $url );

				if ( is_wp_error( $request ) ) {
					return $value;
				}

				$body = wp_remote_retrieve_body( $request );
				$json = json_decode( $body, true );

				$content = $json['graphql']['shortcode_media'];
				$text    = ubisoft_array_traverse( $content['edge_media_to_caption'] );

				$value = wp_json_encode( array(
					'post_id' => $content['shortcode'] ?? null,
					'date'    => $content['taken_at_timestamp'] ?? null,
					'href'    => $value,
					'text'    => $text['text'] ?? null,
					'src'     => $content['display_url'] ?? null,
					'size'    => $content['dimensions'] ?? null,
				) );

			} elseif ( is_twitter_url( $value ) ) {

				$post_id = extract_twitter_post_id( $value );

				$query = '?id=' . $post_id . '&trim_user=true&include_entities=true&include_ext_alt_text=true&include_card_uri=true';

				$body = $this->twitter_client->setGetfield( $query )
				->buildOauth( $this->settings['twitter_url'], 'GET' )
				->performRequest();
				$json = json_decode( $body, true );

				if ( array_key_exists( 'created_at', $json ) ) {

					$date = new DateTime( $json['created_at'] );
					$date = $date->getTimestamp();
				}

				$value = wp_json_encode( array(
					'post_id' => $json['id'] ?? null,
					'date'    => $date ?? null,
					'href'    => $value,
					'text'    => $json['text'] ?? null,
				) );

				return $value;

			}
			return $value;

		}
	}

	new ubisoft_acf_field_hashtag( $this->settings );

endif;

?>
